const uuid = require("uuid");
const MongoClient = require("mongodb").MongoClient;

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// SANDBOX database
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// const connectionUrl = "mongodb+srv://pamperedchef:4B3zMy93IGl7IkEE@shared-v4-lhc6r.mongodb.net/db?retryWrites=true&authSource=admin";

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// TEST database
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// const connectionUrl = "mongodb+srv://pamperedchef:bjaWAIs3ABXjXEVh@shared-v4-ojdag.mongodb.net/db?retryWrites=true&authSource=admin";

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// PROD database
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// const connectionUrl = "mongodb+srv://party:Q1hFyolsTAqZ43auLnYJ@shared-v4-paifn.mongodb.net/db?retryWrites=true&authSource=db";

loadPartyThemes();

async function loadPartyThemes() {
	const mongoClient = MongoClient(connectionUrl);
	await mongoClient.connect();
	const db = mongoClient.db("db");
	const partyThemesCollection = db.collection("partythemes");

	// INSERT NEW PARTY THEMES
	const newPartyThemes = createNewPartyThemes();
	console.log("inserting new partythemes...");
	const result1 = await partyThemesCollection.insertMany(newPartyThemes);
	console.log(`Inserted ${result1.insertedCount} documents into partythemes collection`);

	// DELETE PARTY THEMES
	const partyThemesToDelete = themesToDelete();
	console.log("removing selected party themes...");
	const result2 = await partyThemesCollection.deleteMany(partyThemesToDelete);
	console.log(`Deleted ${result2.deletedCount} documents from partythemes collection`);

	mongoClient.close();
}

function createNewPartyThemes() {
	const newThemes = [];
	newThemes.push({
		themeId: uuid.v4(),
		name: "Cooking With Kids",
		description: "You’re invited to a Cooking with Kids party! Mark your calendar, invite a friend or two, and bring the kids! Get ready to learn delicious recipes and tips on how to cook with kids of all ages.\n\nKids as young as two can help in the kitchen. Don’t miss out on the fun!",
		imageId: "d6a14104-f751-43b0-933b-72ecf43691bf",
		imageUrlSmall: "https://embed.widencdn.net/img/pamperedchef/o6tyoijm6q/600x225px/cover-facebook-pancake-molds-and-emoji-stencils-usca.jpeg?keep=c&crop=yes&u=w8nduy",
		imageUrlLarge: "https://embed.widencdn.net/img/pamperedchef/o6tyoijm6q/1024x384px/cover-facebook-pancake-molds-and-emoji-stencils-usca.jpeg?keep=e&crop=yes&u=w8nduy",
		partyPackUrl: {
			links: [],
		},
	});
	newThemes.push({
		themeId: uuid.v4(),
		name: "Backyard Fun",
		description: "You’re invited to a Backyard Fun party! Mark your calendar and invite a friend or two! Get ready to learn delicious recipes and tips for outdoor dining including main courses, sweet treats, and summertime sips. Don’t miss out on the fun!",
		imageId: "476a8c58-6660-42b4-bc6a-2175fddb7a44",
		imageUrlSmall: "https://embed.widencdn.net/img/pamperedchef/jv8s4hqs3f/600x225px/cover-smores-grilling-usca.jpg?keep=c&crop=yes&u=w8nduy",
		imageUrlLarge: "https://embed.widencdn.net/img/pamperedchef/jv8s4hqs3f/1024x384px/cover-smores-grilling-usca.jpg?keep=e&crop=yes&u=w8nduy",
		partyPackUrl: {
			links: [],
		},
	});
	return newThemes;
}

function themesToDelete() {
	const toDelete = [];
	// Pizza Party
	toDelete.push({"themeId" : "eaa5f15d-cd07-44a5-847d-6d96c2c5982d"});
	// Duplicate new Year New You
	toDelete.push({"themeId" : "df37c5f3-6bb8-4b08-a47b-1697e18db698"});
	return toDelete;
}
